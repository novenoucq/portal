<?php  
    include('header.php'); 
?>
<body class="body-Login-back">

    <div class="container">
       
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center logo-margin ">
              <img src="<?php echo BASE_URL ?>/resources/img/hotel.jpg" alt=""/>
                </div>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                  
                    <div class="panel-heading">
                        <h3 class="panel-title">Porfavor ingresa tus datos</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?php echo BASE_URL ?>/auth/login" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="identity" id="identity" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Recordarme
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                 <button type="submit" class="btn btn-primary">Ingresar</button>
                            </fieldset>
                        </form>
                        <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- Core Scripts - Include with every page -->
    <script src="<?php echo BASE_URL ?>/resources/plugins/jquery-1.10.2.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/metisMenu/jquery.metisMenu.js"></script>

</body>

</html>