<script>

        function marcar(obj, chk) {

            elem = obj.getElementsByTagName('input');

            for (i = 0; i < elem.length; i++)
                elem[i].checked = chk.checked;
        }

        function createUploader() {
            var uploader = new qq.FileUploader ({
                element: document.getElementById('file-uploader'),
                action: '<?php echo base_url(); ?>auth/upload_file',
                debug: true,
                sizeLimit: 0,
                onComplete: function (id, fileName, responseJSON) {
                    if (responseJSON.success == true) {
                        alert('El archivo se ha cargado con éxito');
                        $('#filename_path').val(responseJSON.filename);
                        $('div.qq-upload-button').hide();
                        $('#image_filename_path').html('<img id="theImg" height="350px" width="350px" src="<?php echo base_url(); ?>' + responseJSON.filename + '" />')
                        //$('#submited').show();
                    }
                }
            });
        }
        function CancelUpload() {
            document.forms[0].action = '<?php echo base_url(); ?>auth/cancel_upload';
            document.forms[0].submit();
        }
        // in your app create uploader as soon as the DOM is ready
        // don't wait for the window to load  
        window.onload = createUploader;
    </script>
    <?php
    include('header.php');
    include('menu_bar.php');
    ?>

    <body>
        <div id="page-wrapper">
            <div class="row">
                <!-- page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">ALTA DE USUARIO</h1>
                </div>
                <!--end page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            DATOS DEL USUARIO
                        </div>
                        <div id="infoMessage"><?php echo $message; ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <form role="form" action="<?php echo BASE_URL ?>/auth/create_user" method="post">
                                    <div class="col-lg-6">


                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <?php echo form_input($first_name); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Apellidos</label>
                                            <?php echo form_input($last_name); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Empresa</label>
                                            <?php echo form_input($company); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Correo Eléctronico</label>
                                            <?php echo form_input($email); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Número Telefonico</label>
                                            <?php echo form_input($phone); ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Contraseña</label>
                                            <?php echo form_input($password); ?>
                                        </div>

                                        <div class="form-group">
                                            <label>Confirmar Contraseña</label>
                                            <?php echo form_input($password_confirm); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Foto</label>
                                            <input type="hidden" name="filename_path" id="filename_path" value=""/>
                                            <div id="file-uploader">
                                                <noscript>
                                                <p>Please enable JavaScript</p>
                                                </noscript>
                                            </div>
                                        </div>
                                        <div id="image_filename_path">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Registrar Usuario</button>
                                        <button type="reset" class="btn btn-success">Limpiar Formulario</button>
                                        <?php echo anchor("auth/users/", 'CANCELAR','class="btn btn-danger"'); ?>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                </div>
            </div>
        </div>

    </body>
</html>