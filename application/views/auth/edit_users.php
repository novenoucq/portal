
<?php
include('header.php');
include('menu_bar.php');
?>

<body>
    <div id="page-wrapper">
        <div class="row">
            <!-- page header -->
            <div class="col-lg-12">
                <h1 class="page-header">ACTUALIZAR USUARIO</h1>
            </div>
            <!--end page header -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        DATOS DEL USUARIO
                    </div>
                    <div id="infoMessage"><?php echo $message; ?></div>
                    <div class="panel-body">
                        <div class="row">
                            <?php echo form_open(uri_string()); ?>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>Nombre</label>
                                    <?php echo form_input($first_name); ?>
                                </div>

                                <div class="form-group">
                                    <label>Apellidos</label>
                                    <?php echo form_input($last_name); ?>
                                </div>

                                <div class="form-group">
                                    <label>Empresa</label>
                                    <?php echo form_input($company); ?>
                                </div>

                                <div class="form-group">
                                    <label>Número Telefonico</label>
                                    <?php echo form_input($phone); ?>
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>Contraseña</label>
                                    <?php echo form_input($password); ?>
                                </div>

                                <div class="form-group">
                                    <label>Confirmar Contraseña</label>
                                    <?php echo form_input($password_confirm); ?>
                                </div>
                                
                                <h4>Grupos</h4>
                                <?php foreach ($groups as $group): ?>
                                    <label class="checkbox">
                                        <?php
                                        $gID = $group['id'];
                                        $checked = null;
                                        $item = null;
                                        foreach ($currentGroups as $grp) {
                                            if ($gID == $grp->id) {
                                                $checked = ' checked="checked"';
                                                break;
                                            }
                                        }
                                        ?>
                                        <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>"<?php echo $checked; ?>>
                                        <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                    </label>
                                <?php endforeach ?>

                                <?php echo form_hidden('id', $user->id); ?>
                                <?php echo form_hidden($csrf); ?>

                                <button type="submit" class="btn btn-primary">Actualizar Usuario</button>

                                <?php echo anchor("auth/users/", 'CANCELAR', 'class="btn btn-danger"'); ?>

                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- End Form Elements -->
            </div>
        </div>
    </div>

</body>
</html>