<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PORTAL | HOTEL - ESCUELA AMEALCO</title>
    <!-- Core CSS - Include with every page -->
    <link href="<?php echo BASE_URL ?>/resources/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL ?>/resources/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL ?>/resources/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL ?>/resources/css/style.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL ?>/resources/css/main-style.css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="<?php echo BASE_URL ?>/resources/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
    
    
      <!-- Core Scripts - Include with every page -->
    <script src="<?php echo BASE_URL ?>/resources/plugins/jquery-1.10.2.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/pace/pace.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="<?php echo BASE_URL ?>/resources/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/morris/morris.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/scripts/dashboard-demo.js"></script>
    
    
    <!-- Page-Level Plugin Scripts DataTable-->
    <script src="<?php echo BASE_URL ?>/resources/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo BASE_URL ?>/resources/plugins/dataTables/dataTables.bootstrap.js"></script>
    <link href="<?php echo BASE_URL ?>/resources/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="<?php echo BASE_URL ?>/resources/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    
    
</head>
