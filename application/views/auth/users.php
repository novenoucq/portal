<!DOCTYPE html>
<html>

    <?php
    include('header.php');
    include('menu_bar.php');
    ?>

    <body>

        <div id="page-wrapper">
            <div class="row">
                <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">ADMINISTRACIÓN DE USUARIOS</h1>
                </div>
                <!-- end  page header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           
                            <div id="button_controls">
                                <?php echo anchor("auth/create_user/", 'AGREGAR USUARIO','class="btn btn-info"'); ?>
                                
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>NOMBRE</th>
                                            <th>APELLIDOS</th>
                                            <th>EMAIL</th>
                                            <th>GRUPO</th>
                                            <th>ESTATUS</th>
                                            <th>ACCIÓN</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($users as $user): ?>
                                            <tr class="odd gradeX">
                                                <td class="center"><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td class="center"><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td class="center"><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td class="center">
                                                    <?php foreach ($user->groups as $group): ?>
                                                        <?php echo anchor("#" . $group->id, htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8')); ?><br />
                                                    <?php endforeach ?>
                                                </td>
                                                <td class="center"><?php echo ($user->active) ? anchor("auth/deactivate/" . $user->id, lang('index_active_link'),'class="btn btn-success"') : anchor("auth/activate/" . $user->id, lang('index_inactive_link'),'class="btn btn-danger"'); ?></td>
                                                <td class="center"><?php echo anchor("auth/edit_user/" . $user->id, 'Editar','class="btn btn-info"'); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>

    </body>

</html>
