<?php
include('header.php');
?>

<body class="body-Login-back">
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center logo-margin ">
                <img src="<?php echo BASE_URL ?>/resources/img/hotel.jpg" alt=""/>
            </div>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                  
                    <div class="panel-heading">
                        <h1 class="panel-title">¿Olvidaste tu contraseña?</h1>
                        <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label); ?></p>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?php echo BASE_URL ?>/auth/forgot_password" method="post">
                            <fieldset>
                                
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" id="identity" type="email" autofocus>
                                </div>
                               
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-primary">Recuperar</button>
                                <?php echo anchor(BASE_URL.'/auth/login', 'Cancelar') ?>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>